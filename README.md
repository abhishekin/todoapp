# WeTravel Assignment

This Maven project contains Functional and Performance Tests 
- Functional - Selenium,Java,Cucumber
- Performance - Jmeter 

## Requirements:
Below dependencies needs to be installed/configured
- Preferably Java 8 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (MAVEN_HOME and PATH in environmental variables)
- Download Jmeter and set path variables to run script locally


## Downloading Project:
- Using git command and clone with HTTPS:
  git clone https://abhi00720@bitbucket.org/abhi239/todoapp.git

*or*

- Download the project from https://abhi00720@bitbucket.org/abhi239/todoapp.git

## Test location:
1. Performance Tests can be found at :           /src/test/jmeter
2. Functional Tests(features) can be found at :  /src/test/resources/features


## Execution:

- Export project in any IDE as **File > Open as Maven Project**
- If pulling from GIT, File > New > Paste git url mentioned in "Downloading Project"
- I have used ChromeDriver 98.0.4758.102 which will run on Chrome Version 98, for Chrome version 99, Download Chromedriver at https://chromedriver.chromium.org/downloads and replace in /driver folder 
- Use maven command to download all dependencies and run all tests (Functional, Performance) 
```sh
mvn clean verify
```
For running individual tests,download all Dependencies by **right click on project > Maven > Download Sources** 
- Locally, Feature file can be run as feature by **right click > Run as Feature**
- Jmeter test can be run using command 
```sh
jmeter -n –t src/test/jmeter/loadTest.jmx
```


## Execution reports can be found at
1. Cucumber - /target/cucumber-reports/index.html
2. Jmeter - target/jmeter/reports/LoadTesting/index.html

