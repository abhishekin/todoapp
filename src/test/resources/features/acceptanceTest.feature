Feature: Acceptance test for ToDo App

  @acceptance
  Scenario: Validate ToDoApp functionality
    Given Empty ToDo list
    When I write "buy some milk" to <text-box> and press <enter>
    Then I should see "buy some milk" item on ToDo list

    Given ToDo list with "buy some milk" item
    When I write "love this assignment" to <text-box> and press <enter>
    Then I should see "love this assignment" item added to the ToDo list below "buy some milk" item

    Given ToDo list with "buy some milk" item
    When I click on <checkbox> next to "buy some milk" item
    Then I should see "buy some milk" item marked as DONE

    Given ToDo list with a marked "buy some milk" item DONE
    When I click on <checkbox> next to the item "buy some milk"
    Then I should see "buy some milk" item marked as UNDONE

    Given ToDo list with "get some coffee" item added
    When I click on <delete-button> next to "get some coffee" item
    Then the list should be empty

    Given ToDo list with "get some coffee" item and "drink some water" item in order
    When I click on <delete-button> next to "get some coffee" item
    Then I should see "drink some water" item on ToDo list