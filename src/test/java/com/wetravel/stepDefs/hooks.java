package com.wetravel.stepDefs;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.AppUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class hooks {
    public static WebDriver driver;


    @Before
    public void openBrowser() throws IOException {
        System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
        driver =  new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.get(AppUtils.getGlobalValue("authURL"));
    }

    @After
    public void teardown() {
        driver.quit();
    }

}

