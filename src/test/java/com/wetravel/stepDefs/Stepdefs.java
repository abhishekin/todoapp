package com.wetravel.stepDefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageFactory.TodoPage;

public class Stepdefs {

    TodoPage todoPage;
    public WebDriver driver;

    public Stepdefs() {
        driver = hooks.driver;
        todoPage = new TodoPage(driver);
    }

    //Scenarios
    @Given("Empty ToDo list")
    public void emptyToDoList() {
        Assert.assertTrue(todoPage.verifyEmptyList(driver));

    }

    @When("I write {string} to <text-box> and press <enter>")
    public void iWriteToTextBoxAndPressEnter(String arg0) {
        Assert.assertTrue(todoPage.enterToDoTask(driver, arg0));
        Assert.assertTrue(todoPage.pressEnterKey(driver));

    }

    @Then("I should see {string} item on ToDo list")
    public void iShouldSeeItemOnToDoList(String arg0) {
        Assert.assertTrue(todoPage.verifyTaskIsAvailable(driver, arg0));
    }

    @Given("ToDo list with {string} item")
    public void todoListWithItem(String arg0) {
        Assert.assertTrue(todoPage.verifyTaskIsAvailable(driver, arg0));
    }

    @Then("I should see {string} item added to the ToDo list below {string} item")
    public void iShouldSeeItemAddedToTheToDoListBelowItem(String arg0, String arg1) {
        Assert.assertTrue(todoPage.verifyTaskIsAvailable(driver, arg0));
        Assert.assertTrue(todoPage.verifyTaskIsAvailable(driver, arg1));
    }

    @When("I click on <checkbox> next to {string} item")
    public void iClickOnCheckboxNextToItem(String arg0) {
        Assert.assertTrue(todoPage.selectToDoTask(driver,arg0));
    }

    @Then("I should see {string} item marked as DONE")
    public void iShouldSeeItemMarkedAsDONE(String arg0) {
        Assert.assertTrue(todoPage.verifyTask(driver,arg0,"todo completed"));
    }

    @Given("ToDo list with a marked {string} item DONE")
    public void todoListWithAMarkedItemDONE(String arg0) {
        Assert.assertTrue(todoPage.verifyTask(driver,arg0,"todo completed"));
    }

    @When("I click on <checkbox> next to the item {string}")
    public void iClickOnCheckboxNextToTheItem(String arg0) {
        Assert.assertTrue(todoPage.selectToDoTask(driver,arg0));
    }

    @Then("I should see {string} item marked as UNDONE")
    public void iShouldSeeItemMarkedAsUNDONE(String arg0) {
        Assert.assertTrue(todoPage.verifyTask(driver,arg0,"todo"));

        // Deleting Tasks for next scenario
        Assert.assertTrue(todoPage.deleteTask(driver,"buy some milk"));
        Assert.assertTrue(todoPage.deleteTask(driver,"love this assignment"));
    }

    @And("I delete tasks task {string}")
    public void iDeleteTasksTask(String arg0) {
        Assert.assertTrue(todoPage.deleteTask(driver,arg0));
    }

    @Given("ToDo list with {string} item added")
    public void todoListWithItemAdded(String arg0) {
        Assert.assertTrue(todoPage.enterToDoTask(driver, arg0));
        Assert.assertTrue(todoPage.pressEnterKey(driver));
    }

    @When("I click on <delete-button> next to {string} item")
    public void iClickOnDeleteButtonNextToItem(String arg0) {
        Assert.assertTrue(todoPage.deleteTask(driver,arg0));
    }


    @Then("the list should be empty")
    public void theListShouldBeEmpty() {
        Assert.assertTrue(todoPage.verifyEmptyList(driver));
    }

    @Given("ToDo list with {string} item and {string} item in order")
    public void todoListWithItemAndItemInOrder(String arg0, String arg1) {
        Assert.assertTrue(todoPage.enterToDoTask(driver, arg0));
        Assert.assertTrue(todoPage.pressEnterKey(driver));
        Assert.assertTrue(todoPage.enterToDoTask(driver, arg1));
        Assert.assertTrue(todoPage.pressEnterKey(driver));
    }
}
