package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppUtils {


    public static String getGlobalValue(String key) throws IOException {
        Properties prop = new Properties();
        FileInputStream fs = new FileInputStream("global.properties");
        prop.load(fs);
        return prop.getProperty(key);
    }
}

