package utils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TestUtils {

    public static boolean pressEnter(String strLogicalName, WebElement element) {
        boolean keyPressed = false;
        try {
            element.sendKeys(Keys.RETURN);
            keyPressed = true;
            System.out.println("PASS, Pressed: '" + strLogicalName);
        } catch (Exception e) {
            System.out.println("FAIL, Unable to Press: '" + strLogicalName );
        }

        return keyPressed;
    }

    public static boolean enterValue(WebElement element, String strValue) {
        boolean isValueEntered = false;
        element.clear();
        element.sendKeys(strValue);
        if (element.getAttribute("value").equals(strValue)) {
            isValueEntered = true;
            System.out.println("PASS, Entered '" + strValue + "' in textbox");
        } else {
            System.out.println("FAIL, Unable to enter '" + strValue + "' in textbox");
        }
        return isValueEntered;
    }


    public static boolean verifyToDoItemsCount(List<WebElement> element, int expectedItem) {
        boolean listCorrect = false;
        if(element.size() == expectedItem)
        {
            listCorrect = true;
            System.out.println("PASS, ToDoItems have expected Item" + expectedItem);
        } else {
            System.out.println("FAIL, ToDoItems donot have expected Item" + expectedItem);
        }
        return listCorrect;
    }

    public static boolean verifyTaskIsAvailable(List<WebElement> element, String expectedItem) {
        boolean available;
        if (element.stream().anyMatch(e->e.getText().trim().equals(expectedItem))) {
            available = true;
            System.out.println("PASS, Task: " + expectedItem + " is present in to do list");
        }
        else {
            available = false;
            System.out.println("FAIL, Task: " + expectedItem + " is not present in to do list");
        }
        return available;
    }

    public static boolean verifyTaskNotAvailable(List<WebElement> element, String expectedItem) {
        boolean notAvailable;
        if (element.stream().noneMatch(e->e.getText().trim().equals(expectedItem))) {
            notAvailable = true;
            System.out.println("PASS, Task: " + expectedItem + " is not present in to do list");
        }
        else {
            notAvailable = false;
            System.out.println("FAIL, Task: " + expectedItem + " is present in to do list");
        }
        return notAvailable;
    }

    public static boolean verifyTask(String status, List<WebElement> element1, List<WebElement> element2, String expectedItem) {
        boolean expected = false;
        for (int i =0;i <element1.size();i++) {
            if ((element1.get(i).getText().equals(expectedItem))) {
               if (element2.get(i).getAttribute("class").equals(status)) {
                   expected = true;
                   System.out.println("PASS, Task:" + expectedItem + "is " + status);
                   break;
               }
               else {
                   System.out.println("FAIL, Task:" + expectedItem + "is not " + status);
               }
            }
        }
        return expected;
    }

    public static boolean markTaskDone(List<WebElement> element1, List<WebElement> element2, String taskAdded) {
        boolean isClicked = false;
        for (int i =0;i <element1.size();i++) {
            if ((element1.get(i).getText().equals(taskAdded))) {
                element2.get(i).click();
                isClicked = true;
                System.out.println("PASS, Task " + taskAdded + " marked as Done");
                break;
            }else {
                System.out.println("FAIL, Task " + taskAdded + " not marked as Done");
            }
        }
        return isClicked;
    }

    public static boolean deleteTask(WebDriver driver, List<WebElement> element1, List<WebElement> element2, String taskAdded) {
        boolean deleted = false;
        Actions actions = new Actions(driver);
        for (int i =0;i <element1.size();i++) {
            if ((element1.get(i).getText().equals(taskAdded))) {
                actions.moveToElement(element1.get(i)).perform();
                element2.get(i).click();
                deleted = true;
                System.out.println("PASS, Task deleted " + taskAdded);
                break;
            }else {
                System.out.println("FAIL, Task Not deleted " + taskAdded);
            }
        }
        return deleted;
    }
    public static void waitForElementToBeVisible(WebDriver driver, List<WebElement> element) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(element.get(1)));

    }

    public static void waitForElementToBeClickable(WebDriver driver, WebElement element) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(element));
    }


}
