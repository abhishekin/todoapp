package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.TestUtils;

import java.util.List;


public class TodoPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */
    WebDriver driver;

    @FindBy(xpath = "/html/body/section/header/input")
    WebElement toDotextbox;

    @FindBy(xpath = "/html/body/section/section/ul/li/div/label")
    public List<WebElement> addedTasksNames;

    @FindBy(xpath = "/html/body/section/section/ul/li/div/input")
    public List<WebElement> addedTasksCheckbox;

    @FindBy(xpath = "//div/button[@class='destroy']")
    public List<WebElement> addedTasksDeletion;

    @FindBy(xpath = "/html/body/section/section/ul/li")
    public List<WebElement> taskList;



    public TodoPage(WebDriver driver) {
        this.driver = driver;
        //This initElements method will create  all WebElements
        PageFactory.initElements(driver, this);
    }


    public boolean enterToDoTask(WebDriver driver, String task) {
        TestUtils.waitForElementToBeClickable(driver,toDotextbox);
        return TestUtils.enterValue(toDotextbox, task);
    }


    public boolean pressEnterKey(WebDriver driver) {
        return TestUtils.pressEnter("Enter Key", toDotextbox);
    }

    public boolean selectToDoTask(WebDriver driver, String taskadded) {
        return TestUtils.markTaskDone(addedTasksNames,addedTasksCheckbox,taskadded);
    }


    public boolean deleteTask(WebDriver driver, String taskadded) {;
        return TestUtils.deleteTask(driver, addedTasksNames, addedTasksDeletion,taskadded);
    }

    public boolean verifyEmptyList(WebDriver driver) {
        return TestUtils.verifyToDoItemsCount(addedTasksNames,0);
    }


    public boolean verifyTaskIsAvailable(WebDriver driver, String expectedItems) {
        return TestUtils.verifyTaskIsAvailable(addedTasksNames,expectedItems);
    }

    public boolean verifyTaskNotAvailable(WebDriver driver, String expectedItems) {
        return TestUtils.verifyTaskNotAvailable(addedTasksNames,expectedItems);
    }

    public boolean verifyTask(WebDriver driver, String expectedItems, String status) {
        return TestUtils.verifyTask(status,addedTasksNames,taskList,expectedItems);
    }


}
